function filter(elements, cb) {

    const ans = [];
    for (let index = 0; index < elements.length; index++){
        const result = cb(elements[index]);
    
        if(result!==undefined){
            ans.push(result);
        }
    }

    return ans;
}

module.exports = filter;