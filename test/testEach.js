const each = require("../each");
const items = require("./data");


each(items, function(element, index){
    console.log(`Element: ${element}, Index: ${index}`);
})