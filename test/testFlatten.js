const flatten = require("../flattten");

const nestedArray = [1, [2], [[3]], [[[4, [5, 6]]]]]; // use this to test 'flatten'

const result = flatten(nestedArray);
console.log(result);
