function flatten(elements) {
    const result = [];

    function flattenHelper(arr) {
        try {
            for (let element of arr) {
                if (Array.isArray(element)) {
                    flattenHelper(element);
                } else {
                    result.push(element);
                }
            }
        } catch (error) {
            console.log(`Invalid input. ${error}`);
        }
    }

    flattenHelper(elements);
    return result;
}

module.exports = flatten;
