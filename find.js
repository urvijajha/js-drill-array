function find(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        const result = cb(elements[index], index);

        if (result !== undefined) {
            return result;
        }
    }
    
    return undefined; 
}

module.exports = find;
